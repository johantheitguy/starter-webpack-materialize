export default function printMe() {
    if (process.env.NODE_ENV !== 'production') {
        return "OK" + Date.now();
    }
    return "Updated " + Date.now();
}