
import "./index.css";

import htmlfile from "./index.html";
import printMe from './import.js';
import jQuery from 'jquery';

import Materialize from 'materialize-css';
import { TypeScriptTest } from './import.ts';

const tstest = new TypeScriptTest();

function component() {
  var element = document.createElement('div');
  element.innerHTML = htmlfile;
  return element;
}

document.body.appendChild(component());

setInterval(() => {
  jQuery('.printme').html(printMe() + " " + Date.now());
  jQuery('.typescripttest').html(tstest.getData());
}, 1000);


if (module.hot) {
  module.hot.accept('./import.js', function() {
    console.log('Accepting the updated printMe module!');
    jQuery('div').html(printMe());
  });
}

jQuery(document).ready(() => Materialize.toast('I am a toast!', 4000));
jQuery('a.toastme').click(() => Materialize.toast('I am a toast!', 4000));